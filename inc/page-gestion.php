
<?php
/*
 * odoo.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

global $wpof;
$wpof->odoo_sub_pages =
[
    "search" => __("Recherche"),
    "invoice" => __("Détails d'une facture"),
    "odoo_formation_direct" => __("Factures Odoo formation en direct"),
    "odoo_formation_soustraitance" => __("Factures Odoo formation en sous-traitance"),
    "formateurices" => __("Formateur⋅ices"),
];

function get_page_gestion_odoo($sub_page = null)
{
    global $wpof;
    
    if (!champ_additionnel("formateur_code"))
    {
        $wpof->champs_additionnels['formateur_code'] = 1;
        $wpof->update_meta("champs_additionnels");
    } 
    $html = "";
    if (isset($wpof->uri_params[0]) && $wpof->uri_params[0] == "odoo")
        array_shift($wpof->uri_params);
        
    if (isset($wpof->uri_params[0]) && array_key_exists($wpof->uri_params[0], $wpof->odoo_sub_pages))
        array_shift($wpof->uri_params);
    
    if ($sub_page === null)
        $sub_page = array_key_first($wpof->odoo_sub_pages);
    
    $fonction = "get_tableau_gestion_odoo_".$sub_page;
    $html .= $fonction();
    
    return $html;
}

function get_param_block(Formateur $responsable, array $formateurices, int $formateur_id = null, int $odoo_client_id = null)
{
    global $wpof;
    ob_start();
    ?>
    <div class="white-board edit-data grid-6col">
        <?php
        if (!isset($wpof->annee_comptable))
            init_term_list('annee_comptable');
        echo get_input_jpost($responsable, "annee_comptable", 
        [
            'select' => '',
            'label' => __('Année comptable'),
            'aide' => false,
            'postprocess' => 'page-reload',
            ]); ?>
        <?php if ($formateur_id !== null) : ?>
        <div class="opaga_input">
            <label class="top"><?php _e("Formateur⋅ice"); ?></label>
        <?php $liste_formateurices = [];
        foreach($formateurices as $fid => $f)
        {
            $identite = trim($f->get_identite());
            $liste_formateurices[$fid] = empty($identite) ? "_Pas de nom ".$f->username : $identite;
        }
        asort($liste_formateurices);
        echo select_by_list($liste_formateurices, "liste_formateurices", $formateur_id, 'class="single-select2"', __('Choisissez une personne')); ?>
        </div>
        <?php
        if ($formateur_id > 0) : 
            $liste_client = $wpof->plugin->getClients($formateurices[$formateur_id]);
            $liste_client = $liste_client['morale'] + $liste_client['physique'];
            $odoo_client_id = $odoo_client_id ?? -1;
            ?>
            <div class="opaga_input">
                <label class="top"><?php _e("Client"); ?></label>
                <?php echo select_by_list($liste_client, "liste_clients", $odoo_client_id, 'class="single-select2"', __('Choisissez un client')); ?>
            </div>
        <?php endif; ?>
        <script>
            jQuery(document).ready(function($) 
            {
                $('select[name="liste_formateurices"]').change(function(e)
                {
                    e.preventDefault();
                    if ($(this).val() > 0)
                        document.location.assign('<?php echo home_url().'/'.$wpof->url_gestion.'/odoo/search/?fid='; ?>' + $(this).val());
                });
                $('select[name="liste_clients"]').change(function(e)
                {
                    e.preventDefault();
                    if ($(this).val() > 0)
                        document.location.assign('<?php echo home_url().'/'.$wpof->url_gestion.'/odoo/search/?fid=-1&cid='; ?>' + $(this).val());
                });
            });
        </script>
        <?php endif; ?>
    </div>
    <?php
    return ob_get_clean();
}

function get_factures_table(array $factures)
{
    global $wpof;
    if (empty($factures))
        return "";
    ob_start();
    ?>
    <table class="editable opaga opaga2 datatable">
        <thead>
        <?php foreach(array_keys(reset($factures)) as $key) : ?>
            <th><?php echo str_replace('_', ' ', $key); ?></th>
        <?php endforeach; ?>
        </thead>
        <tbody>
            <?php foreach ($factures as $fact) : ?>
                <?php $is_avoir = $fact['amount_untaxed_signed'] < 0; ?>
                <tr class="<?php if ($is_avoir) : ?>avoir <?php endif; ?>">
                <?php foreach ($fact as $key => $value) : ?>
                    <td> <?php echo format_value($fact, $key, $is_avoir); ?> </td>
                <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php
    return ob_get_clean();
}

function get_id_value($value) : string
{
    if (is_array($value) && isset($value[1]))
        return $value[1];
    else
        return $value;
}

function format_value(array $fact, string $key, bool $is_avoir = false): string
{
    global $wpof;
    $value = $fact[$key];
    $add_html = "";
    if ($key == 'partner_id')
        $add_html = '</br ><a class="bouton" href="'.home_url().'/'.$wpof->url_gestion.'/odoo/search/?cid='.$value[0].
            '" title="'.__('Voir toutes les factures de ce client').'">'.wpof_fa('file-invoice-dollar').'</a>';
    if ($key == 'description' && strlen($value) > 400)
        $value = string_cut($value, 400)." <strong>[…]</strong>";
    if ($key == 'id')
    {
        $text = "";
        if ($is_avoir)
            $text .= '<div class="alerte">'.__("Avoir").'</div>';
        $text .= '<a class="bouton" href="'.home_url().'/'.$wpof->url_gestion.'/odoo/invoice/?id='.$value.'">'.($is_avoir ? __('Cet avoir') : __("Cette facture")).'</a><br />';
        if (!empty($fact['external_id']))
            $text .= '<a class="bouton" href="'.home_url().'/'.$wpof->url_gestion.'/odoo/invoice/?external_id='.$fact['external_id'].'">'.__("Factures liées").'</a><br />';
        $text .= '<a class="bouton" href="'.$wpof->plugin->getFactureUrlBase().'/'.$value.'" target="blank">'.__('Voir sur Odoo').'</a>';
        $value = $text;
    }
    else if (substr($key, -3) == '_id')
        $value = get_id_value($value);
    $html = (is_array($value)) ? implode(', ', $value) : $value;
    $html .= $add_html;
    return $html;
}

function get_tableau_gestion_odoo_search()
{
    global $wpof;
    $responsable = new Formateur(get_current_user_id());
    $formateurices = $wpof->get_formateurs_by_role("um_formateur-trice");
    ob_start();
    $formateur_id = $_GET['fid'] ?? -1;
    $client_id = $_GET['cid'] ?? -1;
    $activite_code = $_GET['aid'] ?? "";

    echo get_param_block($responsable, $formateurices, $formateur_id, $client_id);
    ?>
    
    <div class="white-board" style="width: 100%;">
    <?php
    if ($formateur_id > 0)
    {
        $formateur = new Formateur($formateur_id);
        $activite_code = $formateur->code;
    }

    if ($client_id > 0)
    {
        $clients = $wpof->plugin->getFacturesClientByYear($client_id, $responsable->annee_comptable);
        if (is_array($clients) && !empty($clients))
            echo get_factures_table($clients);
    }
    else if (empty($activite_code) && $formateur_id > 0)
    {
        ?>
        <div class="editable edit-data">
        <p><?php printf(__("%s n'a pas de code d'activité, veuillez le renseigner !"), $formateur->get_identite()); ?></p>
        <?php echo get_input_jpost($formateur, 'code', [ 'input' => 'text', 'label' => "Code d'activité", 'postprocess' => 'page-reload' ]); ?>
        </div>
        <?php
    }
    else if (!empty($activite_code))
    {
        $factures = $wpof->plugin->getFacturesFormateuriceByYear($activite_code, $responsable->annee_comptable);
        ?>
        <?php
        $has_factures = false;
        if (!empty($factures_formation = get_factures_table($factures['formation'])))
        {
            $has_factures = true;
            echo '<h3>'.__('Factures contenant de la formation').'</h3>';
            echo $factures_formation;
        }
        if (!empty($factures_autre = get_factures_table($factures['autre'])))
        {
            $has_factures = true;
            echo '<h3>'.__('Autres factures').'</h3>';
            echo $factures_autre;
        }
        if (!$has_factures) : ?>
            <div class="editable edit-data">
            <p class="erreur"><?php _e('Aucune facture trouvée sur la période'); ?></p>
                <p><?php printf(__("Le code d'activité de %s est peut-être erroné, veuillez le corriger !"), $formateur->get_identite()); ?></p>
                <?php echo get_input_jpost($formateur, 'code', [ 'input' => 'text', 'label' => "Code d'activité", 'postprocess' => 'page-reload' ]); ?>
            </div>
        <?php endif; ?>
    <?php
    } ?>
    </div>
    <?php
    return ob_get_clean();
}

function get_tableau_gestion_odoo_invoice(): string
{
    global $wpof;
    $responsable = new Formateur(get_current_user_id());
    $html = "";
    
    ob_start();
    ?>
    <div class="white-board">
    <label class="top"><?php _e("Numéro de facture"); ?></label>
    <input name="invoice-number" type="text" />
    <input type="button" class="bouton" name="invoice-search" value="<?php _e("Rechercher une facture"); ?>" />
    </div>
    <script>
        jQuery(document).ready(function($) 
        {
            $('input[name="invoice-search"]').click(function(e)
            {
                e.preventDefault();
                number = $('input[name="invoice-number"]').val().trim();
                if (number != "")
                    document.location.assign('<?php echo home_url().'/'.$wpof->url_gestion.'/odoo/invoice/?number='; ?>' + number);
            });
        });
    </script>
    <?php
    $html .= ob_get_clean();

    $field = array_intersect(array_keys($_GET), [ 'id', 'number', 'external_id' ]);
    if (!empty($field))
    {
        $field = $field[0];
        $facture = $wpof->plugin->getFactureDetailsByField($field, '=', $_GET[$field]);
        $total_odoo_ht = 0;
        $total_odoo_ttc = 0;
        ob_start();
        foreach($facture as $fact)
        {
            $lines = $fact['lines'];
            $total_odoo_ht += $fact['amount_untaxed_signed'];
            $total_odoo_ttc += $fact['amount_total_signed'];
            unset($fact['lines']);
            unset($fact['invoice_line_ids']);
            ?>
            <div class="white-board">
            <h3><?php echo ($fact['amount_untaxed_signed'] < 0 ? '<span class="alerte">'.__("Avoir").'</span>' : '<span class="succes">'.__("Facture").'</span>').' '.
            (empty($fact['number']) ? __('Brouillon') : $fact['number']).' – '.get_id_value($fact['partner_id']); ?> 
            <a class="bouton" href="<?php echo $wpof->plugin->getFactureUrlBase().'/'.$fact['id']; ?>" target="blank"><?php _e('Voir sur Odoo'); ?></a></h3>
            <table class="opaga opaga2">
                <?php foreach($fact as $key => $value) : ?>
                    <tr><td><?php echo $key; ?></td><td>
                        <?php if (substr($key, -3) == '_id')
                            echo get_id_value($value);
                        else
                            echo $value; ?>
                    </td></tr>
                <?php endforeach; ?>
            </table>
            <?php if (!empty($lines)) : ?>
            <table class="opaga opaga2">
                <thead>
                <?php foreach(array_keys($lines[0]) as $key) : ?>
                    <th><?php echo $key; ?></th>
                <?php endforeach; ?>
                </thead>
                <tbody>
                    <?php foreach($lines as $line) : ?>
                        <tr>
                        <?php foreach($line as $key => $value) : ?>
                            <td>
                                <?php
                                if (substr($key, -3) == '_id')
                                    echo get_id_value($value);
                                else
                                    echo (is_array($value)) ? implode(', ', $value) : $value;
                                if ($key == 'partner_id')
                                    echo '</br ><a href="'.home_url().'/'.$wpof->url_gestion.'/odoo/search/?fid=-1&cid='.$value[0].
                                        '" title="'.__('Voir toutes les factures de ce client').'">'.wpof_fa('file-invoice-dollar').'</a>';
                                ?>
                            </td>
                        <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
            </div>
            <?php
        }
        $facture_html = ob_get_clean();
        ob_start();
        if (!empty($facture))
        {
            $external_id = trim($facture[0]['external_id']) ?? null;
            if (!empty($external_id))
            {
                list($session_id, $client_id) = explode('-', $external_id);
                $session = new SessionFormation($session_id);
                $client = new Client($session_id, $client_id); ?>
                <div class="white-board">
                <span class="bouton"><?php echo $client->get_a_link(__("Voir les détails du contrat")); ?></span>
                <?php
                if ($client->tarif_total_chiffre != $total_odoo_ht) : ?>
                <p class="erreur"><?php _e("Les totaux dans Odoo et OPAGA ne correspondent pas !"); ?></p>
                <?php endif; ?>
                <table class="opaga">
                    <thead><tr><th></th><th>Total dans OPAGA</th><th>Total dans Odoo</th></tr></thead>
                    <tr><td>HT</td><td><?php echo $client->tarif_total_chiffre; ?></td><td><?php echo $total_odoo_ht ?></td></tr>
                </table>
                </div>
                <?php
            }
            echo $facture_html;
        }
        else
        {
            echo '<p class="erreur">'.__("Aucune facture trouvée").'</p>';
        }
        $html .= ob_get_clean();
    }
    return $html;
}

function get_tableau_gestion_odoo_odoo_formation_direct(string $type = 'direct')
{
    global $wpof;
    $responsable = new Formateur(get_current_user_id());
    $formateurices = $wpof->get_formateurs_by_role("um_formateur-trice");
    ob_start();
    echo get_param_block($responsable, $formateurices);
    ?>
    <div class="white-board" style="width: 100%;">
    <?php
    $factures = $wpof->plugin->getFacturesFormation($responsable->annee_comptable, $type);
    if (is_array($factures) && !empty($factures)) :
        echo get_factures_table($factures);
    else : ?>
        <p class="erreur"><?php printf(__("Pas de factures de formation en %s sur la période !"), $type); ?></p>
    <?php endif; ?>
    </div>
    <?php
    return ob_get_clean();
}

function get_tableau_gestion_odoo_odoo_formation_soustraitance(): string
{
    return get_tableau_gestion_odoo_odoo_formation_direct('soustraitance');
}

function get_tableau_gestion_odoo_formateurices(): string
{
    global $wpof;
    $responsable = new Formateur(get_current_user_id());
    $total_ca = 0;
    ob_start();
    $formateurices = $wpof->plugin->getFormateuricesByYear($responsable->annee_comptable);
    if (is_array($formateurices) && !empty($formateurices)) :
        foreach($formateurices as $fid => $f)
        {
            $total_ca += $formateurices[$fid]['ca'];
            $formateurices[$fid]['ca'] = numf($f['ca'], 2);
            $match = [];
            $code_act = preg_match('/ACT[0-9]*/', $f['activite'], $match);
            $formateurices[$fid]['activite'] = '<a href="'.home_url().'/'.$wpof->url_gestion.'/odoo/search/?aid='.$match[0].'">'.$f['activite'].'</a>';
        }
        echo get_factures_table($formateurices);
    else : ?>
        <p class="erreur"><?php _e("Pas de formateur⋅ices sur la période !"); ?></p>
    <?php endif;
    $tableau = ob_get_clean();
    ?>
    <div class="white-board" style="width: 100%;">
        <p><?php printf(__("Chiffre d'affaire total %s € HT"), numf($total_ca, 2)); ?></p>
    <?php echo $tableau; ?>
    </div>
    <?php
    return ob_get_clean();
}