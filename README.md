# Plugin Odoo pour OPAGA

Permet à OPAGA de se connecter à Odoo, version modifiée par Coopaname.

Copiez le fichier `odoo.php` (ou tout le dossier) dans un dossier nommé `opaga/plugins` à la racine de votre thème WordPress.
