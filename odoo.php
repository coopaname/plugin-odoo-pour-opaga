<?php
/*
 * odoo.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

if (wpof_path === null)
{
    return;
}

require_once(wpof_path . '/class/class-plugin.php');
require_once(wpof_path . '/class/class-erp.php');
require_once(wpof_path . '/vendor/autoload.php');

use Ripcord\Providers\Laravel\Ripcord;

class OdooProvider extends Ripcord
{
    private $default_limit;

    public function __construct($url, $db, $login, $pwd, $limit)
    {
        parent::__construct(['url' => "$url/xmlrpc/2", 'db' => $db, 'user' => $login, 'password' => $pwd]);
        $this->default_limit = $limit;
    }

    public function search_read($model, $params, $fields, $limit = false)
    {
        return $this->_exec(
            $model,
            'search_read',
            $params,
            $fields,
            $limit
        );
    }

    public function read($model, $id, $fields)
    {
        $odoo_res = $this->_exec(
            $model,
            'read',
            $id,
            $fields
        );

        // On prend juste la valeur du premier (et normalement unique) élément du tableau
        return reset($odoo_res);
    }

    private function _exec($model, $type, $params, $fields, $limit = false): array
    {
        $fields_limit = ['fields' => $fields];
        if ($type == 'search_read') {
            $fields_limit['limit'] = (!$limit) ? $this->default_limit : $limit;
        }
        $odoo_res = $this->client->execute_kw(
            $this->db,
            $this->uid,
            $this->password,
            $model,
            $type,
            [$params],
            $fields_limit
        );

        if (isset($odoo_res['faultCode']) || !is_array($odoo_res)) {
            throw new Exception("Impossible d'executer la requete sur Odoo, Résultat de la requête : \n" . print_r($odoo_res, true));
        }

        return $odoo_res;
    }
}

class Odoo extends ERP implements OpagaPlugin
{

    // Le provider pour dialoguer avec Odoo
    private OdooProvider $ripcord;
    private string $url;

    private string $code_direct;
    private string $code_soustraitance;
    private string $code_refacturation;
    private $facture_fields = 
        [
            'number',
            'internal_reference',
            'origin',
            'analytic_account_id',
            'partner_id',
            'date_invoice',
            'start_date',
            'end_date',
            'amount_untaxed_signed',
            'amount_total_signed',
            'description',
            'external_id',
            'state',
        ];
    private array $facture_line_fields =
        [
			//"sequence",
			"product_id",
			"name",
			"quantity",
			//"uom_id",
			"price_unit",
			"discount",
			"invoice_line_tax_ids",
			"price_subtotal",
			"price_total",
			"currency_id",
			"is_country_required",
			"country_id",
        ];

    function __construct()
    {
        $this->url                  = get_option('wpof_odoo_url');
        $this->code_direct          = get_option('wpof_odoo_code_direct');
        $this->code_soustraitance   = get_option('wpof_odoo_code_soustraitance');
        $this->code_refacturation   = get_option('wpof_odoo_code_refacturation');
        try {
            $this->ripcord              = new OdooProvider($this->url, get_option('wpof_odoo_database'), get_option('wpof_odoo_username'), get_option('wpof_odoo_password'), $this->limit);
        }
        catch (Exception $e) {
            $this->enabled = false;
        }
        $this->plugin_path = dirname(__FILE__);
        $this->plugin_url = get_stylesheet_directory_uri()."/opaga/plugins/". str_replace(dirname(__FILE__, 2), "", dirname(__FILE__, 1));
        global $wpof;
        $wpof->pages_gestion['odoo'] = __("Odoo");
    }

    /**
     * Crée un identifiant qui sera stocké dans Odoo pour identifier les factures correspondant au client pour la session.
     * L'identifiant est préfixé par acompte pour les acomptes (ou facture intermediaires) et par facture pour la facture finale.
     */
    private function _buildExternalId(Client $client, SessionFormation $session, string $prefix = "") {
        $base = $session->id . '-' . $client->id;
        if (in_array($prefix, array("facture", "acompte")))
            return $prefix."-".$base;
        else
            return $base;
    }

    public static function getPluginName()
    {
        return 'Odoo';
    }

    public static function getPluginDescription()
    {
        return "Connecteur à l'ERP Odoo (clients, factures, devis, etc.)";
    }

    public static function getOptions()
    {
        return ['wpof_odoo_url', 'wpof_odoo_database', 'wpof_odoo_username', 'wpof_odoo_password', 'wpof_odoo_code_direct', 'wpof_odoo_code_soustraitance', 'wpof_odoo_code_refacturation'];
    }

    public static function loadAdmin()
    {
        ?>
        <div class="m-5">
            <h3><?php _e('Configuration de la connexion à Odoo'); ?></h3>
            <div>
                <p>
                    URL :
                    <input type="text" name="wpof_odoo_url" value="<?php echo get_option('wpof_odoo_url'); ?>">
                </p>
                <p>
                    <?php _e('Base de données :'); ?>
                    <input type="text" name="wpof_odoo_database" value="<?php echo get_option('wpof_odoo_database'); ?>">
                </p>
                <p>
                    <?php _e("Nom d'utilisateur :"); ?>
                    <input type="text" name="wpof_odoo_username" value="<?php echo get_option('wpof_odoo_username'); ?>">
                </p>
                <p>
                    <?php _e('Mot de passe :'); ?>
                    <input type="password" name="wpof_odoo_password" value="<?php echo get_option('wpof_odoo_password'); ?>">
                </p>
                <p>
                    <?php _e("Code de produit formation en direct"); ?>
                    <input type="text" name="wpof_odoo_code_direct" value="<?php echo get_option('wpof_odoo_code_direct'); ?>">
                </p>
                <p>
                    <?php _e("Code de produit formation en sous-traitance"); ?>
                    <input type="text" name="wpof_odoo_code_soustraitance" value="<?php echo get_option('wpof_odoo_code_soustraitance'); ?>">
                </p>
                <p>
                    <?php _e("Code de produit de refacturation"); ?>
                    <input type="text" name="wpof_odoo_code_refacturation" value="<?php echo get_option('wpof_odoo_code_refacturation'); ?>">
                </p>
            </div>
        </div>
        <?php
    }

    public function canGetClients(): bool
    {
        return true;
    }

    public function getClients(Formateur $formateurice, string $entite_client = null): array
    {
        $res = [Client::ENTITE_PERS_MORALE => [], Client::ENTITE_PERS_PHYSIQUE => []];

        if (is_null($entite_client) || $entite_client == Client::ENTITE_PERS_MORALE) {
            $clients_moral = $this->ripcord->search_read('res.partner.analytic.customer', [["activity.act_code", "=", $formateurice->code], ["partner.parent_id", "=", false], ['partner.is_company', '=', true]], ["id", "partner"]);
            foreach ($clients_moral as $odoo_client) {
                $res[Client::ENTITE_PERS_MORALE][$odoo_client['partner'][0]] = $odoo_client['partner'][1];
            }
        }

        if (is_null($entite_client) || $entite_client == Client::ENTITE_PERS_PHYSIQUE) {
            $clients_physique = $this->ripcord->search_read('res.partner.analytic.customer', [["activity.act_code", "=", $formateurice->code], ["partner.parent_id", "=", false], ['partner.is_company', '=', false]], ["id", "partner"]);
            foreach ($clients_physique as $odoo_client) {

                $res[Client::ENTITE_PERS_PHYSIQUE][$odoo_client['partner'][0]] = $odoo_client['partner'][1];
            }
        }

        return $res;
    }

    public function getCreateClientUrl(SessionFormation $session, string $msg = 'Créez le !'): string
    {
        return '<a href="' . $this->url . '/my/customers/new" target="_blank" class="close">' . $msg . '</a>';
    }

    public function getFacturesFormateuriceByYear(string $code, string $year): array
    {
        $toutes_factures = [ 'formation' => [], 'autre' => [] ];
        $factures_formation = $this->ripcord->search_read('account.invoice', [["analytic_account_id.act_code", "ilike", $code], ["invoice_line_ids.product_id.id", "in", [ $this->code_direct, $this->code_soustraitance ]], ["date", ">=", $year.'-01-01'], ['date', '<=', $year.'-12-31']], $this->facture_fields);
        foreach($factures_formation as $fact)
            $toutes_factures['formation'][$fact['id']] = $fact;

        $factures_autre = $this->ripcord->search_read('account.invoice', [["analytic_account_id.act_code", "ilike", $code], ["date", ">=", $year.'-01-01'], ['date', '<=', $year.'-12-31']], $this->facture_fields);
        foreach($factures_autre as $fact)
            if (!isset($toutes_factures['formation'][$fact['id']]))
                $toutes_factures['autre'][$fact['id']] = $fact;

        return $toutes_factures;
    }

    public function getFacturesClientByYear(int $client_id, string $year): array
    {
        $factures = $this->ripcord->search_read('account.invoice', [["partner_id", "child_of", $client_id], ["date", ">=", $year.'-01-01'], ['date', '<=', $year.'-12-31']], $this->facture_fields);

        return $factures;
    }

    public function getFormateuricesByYear(string $year)
    {
        $factures = $this->getFacturesFormation($year);
        $formateurices = [];
        foreach($factures as $fact)
        {
            $idx = $fact['analytic_account_id'][0];
            $formateurices[$idx] =
            [
                'activite' => $fact['analytic_account_id'][1],
                'ca' => $fact['amount_untaxed_signed'] + ($formateurices[$idx]['ca'] ?? 0)
            ];
        }
        return $formateurices;
    }

    public function getFacturesFormation(string $year, string $type = '') : array
    {
        $factures = [];
        if (!empty($this->{"code_".$type}))
        {
            $id_product = $this->{"code_".$type};
            $factures = $this->ripcord->search_read('account.invoice', [["invoice_line_ids.product_id.id", "=", $id_product], ["date", ">=", $year.'-01-01'], ['date', '<=', $year.'-12-31']], $this->facture_fields);
        }
        else
            $factures = $this->ripcord->search_read('account.invoice', [["invoice_line_ids.product_id.id", "in", [ $this->code_direct, $this->code_soustraitance ]], ["date", ">=", $year.'-01-01'], ['date', '<=', $year.'-12-31']], $this->facture_fields);
        return $factures;
    }

    public function getFactureDetailsByField(string $field, string $op, mixed $value): array
    {
        $facture = [];
        if (in_array($field, [ 'id', 'number', 'external_id']))
        {
            $this->facture_fields[] = 'invoice_line_ids';
            $facture = $this->ripcord->search_read('account.invoice', [[$field, $op, $value]], $this->facture_fields);
            foreach($facture as $idx => $fact)
            {
                $facture[$idx]['lines'] = $this->ripcord->search_read('account.invoice.line', [["id", "in", $fact['invoice_line_ids']]], $this->facture_line_fields);
            }
        }
        return $facture;
    }

    public function haveFactureLink(Client $client): bool
    {
        return $this->enabled && !empty($client->external_id);
    }

    public function getConnectionButton(int $session_id, int $client_id): String
    {
        ob_start();
        ?>
        <div class="dynamic-dialog icone-bouton" style="--colorBoutonBackground: var(--nv-secondary-accent);color: var(--colorBoutonText);" data-function="select_client" data-clientid="<?php echo $client_id; ?>" data-sessionid="<?php echo $session_id; ?>">
            <?php the_wpof_fa('link'); ?>
            <?php _e("Connecter ce client à Odoo"); ?>
        </div>
        <?php
        return ob_get_clean();
    }

    public function getCreateFactureUrl(Client $client, SessionFormation $session, float $total, bool $is_acompte, bool $is_solde = false, array $options_params = []): string|bool
    {
        $newline_char = html_entity_decode("&#10;");
        $mention_nb_stagiaires = "";
        if ($client->nb_confirmes > 0)
            $mention_nb_stagiaires = "Pour ".$client->nb_confirmes." stagiaire".(($client->nb_confirmes > 1) ? "s" : "").$newline_char;
        $lieu = $session->get_lieu_infos();
        if (empty($lieu))
        {
            if ($client->financement == Client::FINANCEMENT_OPAC)
                $lieu_row = "";
            else
                $lieu_row = "Lieu : ".__("À définir ultérieurement").$newline_char;
        }
        else
            $lieu_row = "Lieu : ".$lieu.$newline_char;

        $http_params = [
            'description' => __("Session de formation professionnelle")." « ".$session->get_displayname().' »'.$newline_char
                .$client->dates_texte.$newline_char
                .'Durée '.$client->nb_heure_estime_decimal.' heures'.$newline_char
                .$mention_nb_stagiaires
                .$lieu_row,
            'external_id' => $this->_buildExternalId($client, $session, $is_acompte),
        ] + $options_params;

        $today   = date('Y-m-d', time());
        $product = $client->financement == Client::FINANCEMENT_OPAC ? $this->code_soustraitance : $this->code_direct;

        if (!$is_solde) {
            if ($is_acompte) {
                $http_params['description']  = __('ACOMPTE').' – '.$http_params['description'];
                $http_params['line_description'] = __('Acompte');
            }
            else
                $http_params['line_description'] = __('Facture intermédiaire');
            $http_params['line_price_unit']  = $total;
            $http_params['line_quantity']    = 1;
            $http_params['line_product']     = $product;
            $http_params['start_date']       = $today;
            $http_params['end_date']         = $today;
        } else {
            $line_description = $session->get_displayname();
            if ($client->tarif_total_autres_chiffre > 0)
                $total -= $client->tarif_total_autres_chiffre;
            $line_quantity    = $client->nb_heure_estime_decimal;
            $line_price       = round($total / $line_quantity, 2);
            if ($line_quantity * $line_price != $total) {
                $line_description = $line_description . ' (' . number_format($line_quantity, 2, ',', ' ') . " heures)";
                $line_quantity    = 1;
                $line_price       = $total;
            }
            $http_params['line_description_1'] = $line_description;
            $http_params['line_price_unit_1']  = $line_price;
            $http_params['line_quantity_1']    = $line_quantity;
            $http_params['line_product_1']     = $product;

            // si des frais non pédagogiques sont mentionnés dans OPAGA, il faut les reporter dans la facture
            if ($client->tarif_total_autres_chiffre > 0)
            {
                $http_params['line_description_2'] = $client->autres_frais;
                $http_params['line_price_unit_2']  = $client->tarif_total_autres_chiffre;
                $http_params['line_quantity_2']    = 1;
                $http_params['line_product_2']     = $this->code_refacturation;
                $index_next_line = 3;
            }
            else
                $index_next_line = 2;

            $http_params['start_date']         = date('Y-m-d', $session->first_date_timestamp);
            $http_params['end_date']           = date('Y-m-d', $session->last_date_timestamp);
            $deja_facture                      = $this->getFacturesClient($client, $session)['factures'];
            // S'il y a eu d'autres factures après l'éventuel acompte ce sont des factures intermédiaires
            foreach ($deja_facture as $index => $facture) {
                $line_number = $index + $index_next_line;
                $line_description = 'Facture intermédiaire N°' . $facture['num_facture'];
                $line_montant = $facture['montant_ht'];
                // On retire le montant des acomptes de la facture finale avec la quantité à -1
                $line_quantity = -1;

                // Si un montant d'acompte est défini, c'est que la première facture est une facture d'acompte
                if ($client->acompte > 0 && $index == 0)
                {
                    $line_description = 'Acompte N°' .  $facture['num_facture'];
                }

                // Cas d'un avoir sur un des acomptes / factures intermédiaires
                if ($line_montant < 0) 
                {
                    $line_description = 'Avoir N°' . $facture['num_facture'];
                    $line_montant = abs($facture['montant_ht']);
                    $line_quantity = 1;
                }

                $http_params['line_description_' . $line_number] = $line_description;
                $http_params['line_price_unit_' . $line_number]  = $line_montant;
                $http_params['line_quantity_' . $line_number]    = $line_quantity;
                $http_params['line_product_' . $line_number]     = $product;
            }
        }

        return $this->url . '/my/client-invoices/new?customer_id=' . $client->external_id . '&' . http_build_query($http_params);
    }

    public function getFacturesClient(Client $client, SessionFormation $session, bool $is_acompte = true): array
    {
        $res = [
            'montant_total_ht'  => 0,
            'montant_total_ttc' => 0,
            'monnaie'           => '€',
            'factures'          => []
        ];

        $base_external_id = $this->_buildExternalId($client, $session);
        // Pour les factures intermédiaires on prend celles qui sont validées par la compta, pour la définitive on prend celle qui n'est pas refused (peut être en brouillon par exemple)
        $state_condition = $is_acompte ? ['account_validation_state', '=', [ 'account_validated', 'wait_account_validation']] : ['account_validation_state', '!=', 'refused'];
        // On prend les factures dont l'external_id correspond. On inverse l'ordre pour aller de la plus ancienne à la plus récente.
        $factures = array_reverse($this->ripcord->search_read('account.invoice', [['external_id', 'like', $base_external_id], $state_condition], ['amount_untaxed_signed', 'amount_total_signed', 'number', 'description']));
        foreach ($factures as $facture) {
            $montant_ht               = $facture['amount_untaxed_signed'];
            $montant_ttc              = $facture['amount_total_signed'];
            $res['montant_total_ht'] += $montant_ht;
            $res['montant_total_ttc'] += $montant_ttc;
            $res['factures'][]        = [
                'num_facture' => $facture['number'],
                'description' => $facture['description'],
                'montant_ht'  => $montant_ht,
                'montant_ttc' => $montant_ttc,
                // Si c'est un avoir, le numéro de la facture qui lui correspond, false sinon.
                'num_orig' => (isset($facture['origin'])) ? $facture['origin'] : false,
            ];
        }

        return $res;
    }

    public function getFacturesClientUrl(Client $client, SessionFormation $session, bool $is_acompte = true): string
    {
        $external_id = $this->_buildExternalId($client, $session, $is_acompte);
        return $this->url . '/my/client-invoices/?search_in=external_id&' . http_build_query(['search' => $external_id]);
    }

    public function getFactureUrlBase(): string
    {
        return $this->url.'/my/invoices';
    }

    public function getClientInfos(string $client_id): array
    {
        // Le résultat. Au mnimum on a contact et nom.
        $res = [
            'nom'     => '',
            'contact' => []
        ];

        $parent_partner = $this->ripcord->read(
            'res.partner',
            (int) $client_id,
            [
                "child_ids",
                "company_type",
                "display_name",
                "firstname",
                "lastname",
                "parent_id",
                "company_name",
                "phone",
                "mobile",
                "email",
                "vat",
                "comment",
                "customer",
                "user_id",
                "siret",
            ]
        );

        $adresses_partner = $this->ripcord->search_read(
            'res.partner',
            [['type', '=', 'invoice'], ['id', 'in', $parent_partner['child_ids']]],
            [
                "street",
                "street2",
                "zip",
                "city",
                "country_id",
            ]
        );

        foreach ($adresses_partner as $adresse) {
            $res['contact'][] = [
                'adresse'     => $adresse['street'] . "\n" . $adresse['street2'],
                'code_postal' => $adresse['zip'],
                'ville'       => $adresse['city'],
                'pays'        => $adresse['country_id'][1],
                'telephone'   => $parent_partner['phone'] ? $parent_partner['phone'] : $parent_partner['mobile'],
            ];
        }

        if ($parent_partner['company_type'] == 'person') {
            $res['nom']    = $parent_partner['lastname'];
            $res['prenom'] = $parent_partner['firstname'];
            $res['email']  = $parent_partner['email'];
        } else {
            $res['nom']   = $parent_partner['display_name'];
            $res['siret'] = $parent_partner['siret'];
            $res['tva']   = $parent_partner['vat'];
        }

        return $res;
    }


    public function createContrat(Contrat $contrat)
    {

    }

    public function initGestionPage(): void
    {
        global $wpof;
        $wpof->plugin = $this;
        add_action('wp_enqueue_scripts', function() { wp_enqueue_style('odoo-gestion', $this->plugin_url."/css/odoo-gestion.css");}, 22);
        require_once $this->plugin_path . '/inc/page-gestion.php';
    }
}
